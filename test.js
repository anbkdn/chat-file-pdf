const { createClient } = require('@supabase/supabase-js');

// Replace with your Supabase API URL and API Key
const supabaseUrl = 'YOUR_SUPABASE_API_URL';
const supabaseKey = 'YOUR_SUPABASE_API_KEY';

// Initialize the Supabase client
const supabase = createClient('https://bozltyjuwtyhrchjrpgz.supabase.co', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImJvemx0eWp1d3R5aHJjaGpycGd6Iiwicm9sZSI6ImFub24iLCJpYXQiOjE2OTU4Mjk1ODMsImV4cCI6MjAxMTQwNTU4M30.d5H0vkzPr8r83PwjLBQI9qmNB9jToH_0HL6GZOeIyyw');

// Your vector data to be saved
const vectorData = {
  x: 1.0,
  y: 2.0,
  z: 3.0,
};

// Name of the table where you want to save the vector
const tableName = 'vectors';

// Insert the vector data into the specified table
async function saveVector() {
    try {
        const data =  await supabase
          .from(tableName)
          .upsert([vectorData]);
          console.log(data)
          return data
        
    } catch (error) {
        console.log("ERROR:", error)
    }
}

// Call the saveVector function to save the vector data
saveVector()
