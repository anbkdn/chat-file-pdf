import {SupabaseFilterRPCCall, SupabaseVectorStore} from "langchain/vectorstores/supabase";
import {createClient} from "@supabase/supabase-js";
import {Document} from "langchain/dist/document";
import {OPENAI_TYPE, SUPABASE_KEY, SUPABASE_URL} from "@/utils/app/const";
import {getEmbeddings} from "@/utils/embeddings";
import {KeyConfiguration, ModelType, UserAgent} from "@/types";
import OpenAI from "openai";
import {OpenAIEmbeddings} from "langchain/embeddings/openai";


const client = createClient('https://bozltyjuwtyhrchjrpgz.supabase.co', SUPABASE_KEY!);

export const getVectorStore = async (keyConfiguration: KeyConfiguration, texts: string[], metadata: object) => {
    return await SupabaseVectorStore.fromTexts(texts, metadata, await getEmbeddings(keyConfiguration),
        {
            client,
            tableName: "documents",
            queryName: "match_documents",
        }
    );
}

export const getExistingVectorStore = async (keyConfiguration: KeyConfiguration, fileName: string) => {

    // This is filter by match documents
    const fileNameFilter: SupabaseFilterRPCCall = (rpc) =>
        rpc.filter("metadata->>file_name", "eq", fileName);

    return await SupabaseVectorStore.fromExistingIndex(await getEmbeddings(keyConfiguration),
        {
            client,
            tableName: "documents",
            queryName: "match_documents",
            filter:fileNameFilter
        }
    );
}

export const saveUserAgent = async (data: UserAgent) => {
  return client
    .from('trackingips')
    .insert(data)
}

export const getUserAgent = async (ua: string | undefined) => {
  return client.from('trackingips').select().eq('name', ua)
}

export const updateUserAgent = async (data: any) => {
  return client.from('trackingips').update(data).eq('name', data.name)
}

export const saveEmbeddings = async (keyConfiguration: KeyConfiguration, documents: Document[]) => {
    const openai = new OpenAI({apiKey: keyConfiguration.apiKey})
    const embeddings = new OpenAIEmbeddings({openAIApiKey: keyConfiguration.apiKey});

    const store = new SupabaseVectorStore(embeddings, {
        client,
        tableName: "documents",
        queryName: "match_documents"
    });
    await store.addDocuments(documents);
    return {
        ok: 'ok'
    }
    // const supabaseVectorStore = new SupabaseVectorStore(await getEmbeddings(keyConfiguration),
    //     {client, tableName: "documents",
    //     });
    //
    // console.log({supabaseVectorStore})
    //
    // // wait for https://github.com/hwchase17/langchainjs/pull/1598 to be released
    // if (keyConfiguration.apiType === ModelType.AZURE_OPENAI) {
    //     for (const doc of documents) {
    //         await supabaseVectorStore.addDocuments([doc]);
    //     }
    // } else {
    //     await supabaseVectorStore.addDocuments(documents);
    // }

}