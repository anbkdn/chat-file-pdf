import OpenAI from 'openai';

// Initialize the OpenAI API client with your API key
const apiKey = 'YOUR_OPENAI_API_KEY';
const openai = new OpenAI.OpenAIApi({
    apiKey
})
// Sample text to embed
const textToEmbed = 'This is a sample document for embedding.';

// Generate embeddings using OpenAI's language model
async function generateEmbedding() {
    try {
        const response = await openai.embed({
            model: 'text-davinci-002', // You can choose an appropriate model
            documents: [textToEmbed],
        });

        const embeddings = response.data.embeddings;
        console.log('Document Embeddings:', embeddings);
    } catch (error) {
        console.error('Error generating embeddings:', error);
    }
}
