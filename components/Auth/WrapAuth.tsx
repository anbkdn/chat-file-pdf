import React, {useEffect, useState} from 'react';
import { useRouter } from 'next/router';

export  const requireAuth = <P extends object>(
  WrappedComponent: React.ComponentType<P>
) => {
  // eslint-disable-next-line react/display-name
  return (props: P) => {
    const [isAdmin, setIsAdmin] = useState(false)
    const handleVerify = async (token: string | null) => {
      let response : Response

      response = await fetch('/api/verify', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`,
        },
      });

      if(!response.ok) {
        await  router.push('/')
      }

      setIsAdmin(true)
    }
    const router = useRouter();

    useEffect(() => {
      if (typeof window !== 'undefined') {
        // Code that uses localStorage
        const token = localStorage.getItem('token');
      if (!token) {
        router.push('/');
      } else {
        handleVerify(token).then()
      }
      }
    }, [router]);
    return <WrappedComponent {...props} isAdmin/>;
  };
};
