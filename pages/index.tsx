import {Chat} from '@/components/Chat/Chat';
import {Navbar} from '@/components/Mobile/Navbar';
import {ChatFolder, Conversation, KeyConfiguration, KeyValuePair, Message, ModelType,} from '@/types';
import {cleanConversationHistory, cleanSelectedConversation,} from '@/utils/app/clean';
import {DEFAULT_SYSTEM_PROMPT} from '@/utils/app/const';
import {saveConversation, saveConversations, updateConversation,} from '@/utils/app/conversation';
import {saveFolders} from '@/utils/app/folders';
import {exportData, importData} from '@/utils/app/importExport';
import {GetServerSideProps} from 'next';
import {useTranslation} from 'next-i18next';
import {serverSideTranslations} from 'next-i18next/serverSideTranslations';
import Head from 'next/head';
import React, {useCallback, useEffect, useRef, useState} from 'react';

interface HomeProps {
    homeServerSideApiKeyIsSet: boolean;
    isAdmin: boolean
}

const Home: React.FC<HomeProps> = ({homeServerSideApiKeyIsSet, isAdmin}) => {
    const {t} = useTranslation('chat');
    const [conversations, setConversations] = useState<Conversation[]>([]);
    const [selectedConversation, setSelectedConversation] =
        useState<Conversation>();
    const [loading, setLoading] = useState<boolean>(false);
    const [lightMode, setLightMode] = useState<'dark' | 'light'>('dark');
    const [messageIsStreaming, setMessageIsStreaming] = useState<boolean>(false);
    const [messageError, setMessageError] = useState<boolean>(false);
    const [currentMessage, setCurrentMessage] = useState<Message>();
    const [showKeyConfigurationAlert, setShowKeyConfigurationAlert] = useState(false);
    const [keyConfiguration, setKeyConfiguration] = useState<KeyConfiguration>({
        apiType: ModelType.OPENAI,
        apiKey: '',
        apiModel: 'gpt-3.5-turbo',
        azureApiKey: '',
        azureInstanceName: '',
        azureApiVersion: '',
        azureDeploymentName: '',
        azureEmbeddingDeploymentName: '',
    });
    const stopConversationRef = useRef<boolean>(false);

    const handleKeyConfigurationValidation = useCallback((): boolean => {
        if (!homeServerSideApiKeyIsSet && !keyConfiguration.apiKey && !keyConfiguration.azureApiKey) {
            setShowKeyConfigurationAlert(true);
            return false;
        }
        return true;
    },[keyConfiguration.apiKey, keyConfiguration.azureApiKey, homeServerSideApiKeyIsSet])

    const handleSend = useCallback(async (message: Message, deleteCount = 0) => {
        if (!handleKeyConfigurationValidation()) {
            return;
        }
        if (selectedConversation) {
            let updatedConversation: Conversation;

            if (deleteCount) {
                const updatedMessages = [...selectedConversation.messages];
                for (let i = 0; i < deleteCount; i++) {
                    updatedMessages.pop();
                }

                updatedConversation = {
                    ...selectedConversation,
                    messages: [...updatedMessages, message],
                };
            } else {
                updatedConversation = {
                    ...selectedConversation,
                    messages: [...selectedConversation.messages, message],
                };
            }

            setSelectedConversation(updatedConversation);
            setLoading(true);
            setMessageIsStreaming(true);
            setMessageError(false);


            const controller = new AbortController();

            let response: Response;

            response = await fetch(
            `/api/query?message=${message.content}&indexName=${updatedConversation.index.indexName}`, {
              method: 'GET',
              headers: {
                'x-api-type': keyConfiguration.apiType ?? '',
                'x-api-key': keyConfiguration.apiKey ?? '',
                'x-api-model': keyConfiguration.apiModel ?? '',
                'x-azure-api-key': keyConfiguration.azureApiKey ?? '',
                'x-azure-instance-name': keyConfiguration.azureInstanceName ?? '',
                'x-azure-api-version': keyConfiguration.azureApiVersion ?? '',
                'x-azure-deployment-name': keyConfiguration.azureDeploymentName ?? '',
                'x-azure-embedding-deployment-name': keyConfiguration.azureEmbeddingDeploymentName ?? '',
              },
            });

            if (!response.ok) {
                setLoading(false);
                setMessageIsStreaming(false);
                setMessageError(true);

                const message = await response.text();
                console.log('chat failed: ', message);
                alert(`error message: ' ${message}`);
                return;
            }

            if (!response?.body) {
                setLoading(false);
                setMessageIsStreaming(false);
                setMessageError(true);
                return;
            }

            if (updatedConversation.messages.length === 1) {
                const {content} = message;
                const customName =
                    content.length > 30 ? content.substring(0, 30) + '...' : content;

                updatedConversation = {
                    ...updatedConversation,
                    name: customName,
                };
            }

            setLoading(false);

            const reader = response.body.getReader();
            const decoder = new TextDecoder();
            let done = false;
            let isFirst = true;
            let text = '';

            while (!done) {
                if (stopConversationRef.current) {
                    controller.abort();
                    done = true;
                    break;
                }
                const {value, done: doneReading} = await reader.read();
                done = doneReading;
                const chunkValue = decoder.decode(value);

                text += chunkValue;

                if (isFirst) {
                    isFirst = false;
                    const updatedMessages: Message[] = [
                        ...updatedConversation.messages,
                        {role: 'assistant', content: chunkValue},
                    ];

                    updatedConversation = {
                        ...updatedConversation,
                        messages: updatedMessages,
                    };

                    setSelectedConversation(updatedConversation);
                } else {
                    const updatedMessages: Message[] = updatedConversation.messages.map(
                        (message, index) => {
                            if (index === updatedConversation.messages.length - 1) {
                                return {
                                    ...message,
                                    content: text,
                                };
                            }

                            return message;
                        },
                    );

                    updatedConversation = {
                        ...updatedConversation,
                        messages: updatedMessages,
                    };

                    setSelectedConversation(updatedConversation);
                }
            }
            await reader.cancel();

            saveConversation(updatedConversation);

            const updatedConversations: Conversation[] = conversations.map(
                (conversation) => {
                    if (conversation.id === selectedConversation.id) {
                        return updatedConversation;
                    }

                    return conversation;
                },
            );

            if (updatedConversations.length === 0) {
                updatedConversations.push(updatedConversation);
            }

            console.log(`handle chat question: ${message}, response: ${text}`);
            setConversations(updatedConversations);

            saveConversations(updatedConversations);

            setMessageIsStreaming(false);
        }
    },[conversations, handleKeyConfigurationValidation, keyConfiguration.apiKey, keyConfiguration.apiModel, keyConfiguration.apiType, keyConfiguration.azureApiKey, keyConfiguration.azureApiVersion, keyConfiguration.azureDeploymentName, keyConfiguration.azureEmbeddingDeploymentName, keyConfiguration.azureInstanceName, selectedConversation]);

    const handleNewConversation = () => {
        const lastConversation = conversations[conversations.length - 1];

        const newConversation: Conversation = {
            id: lastConversation ? lastConversation.id + 1 : 1,
            name: `${t('Conversation')} ${
                lastConversation ? lastConversation.id + 1 : 1
            }`,
            messages: [],
            prompt: DEFAULT_SYSTEM_PROMPT,
            folderId: 0,
            index: {
                indexName: '',
                indexType: '',
            },
        };

        const updatedConversations = [...conversations, newConversation];

        setSelectedConversation(newConversation);
        setConversations(updatedConversations);

        saveConversation(newConversation);
        saveConversations(updatedConversations);

        setLoading(false);
    };

    const handleUpdateConversation = (
        conversation: Conversation,
        data: KeyValuePair,
    ) => {
        const updatedConversation = {
            ...conversation,
            [data.key]: data.value,
        };

        const {single, all} = updateConversation(
            updatedConversation,
            conversations,
        );

        setSelectedConversation(single);
        setConversations(all);
    };

    const handleEditMessage = (message: Message, messageIndex: number) => {
        if (selectedConversation) {
            const updatedMessages = selectedConversation.messages
                .map((m, i) => {
                    if (i < messageIndex) {
                        return m;
                    }
                })
                .filter((m) => m) as Message[];

            const updatedConversation = {
                ...selectedConversation,
                messages: updatedMessages,
            };

            const {single, all} = updateConversation(
                updatedConversation,
                conversations,
            );

            setSelectedConversation(single);
            setConversations(all);

            setCurrentMessage(message);
        }
    };

    useEffect(() => {
        if (currentMessage) {
            handleSend(currentMessage);
            setCurrentMessage(undefined);
        }
    }, [currentMessage, handleSend]);


    useEffect(() => {
        const theme = localStorage.getItem('theme');
        if (theme) {
            setLightMode(theme as 'dark' | 'light');
        }

        const keyConfiguration = localStorage.getItem('keyConfiguration');
        if (keyConfiguration) {
            setKeyConfiguration(JSON.parse(keyConfiguration));
        }

        const conversationHistory = localStorage.getItem('conversationHistory');
        if (conversationHistory) {
            const parsedConversationHistory: Conversation[] =
                JSON.parse(conversationHistory);
            const cleanedConversationHistory = cleanConversationHistory(
                parsedConversationHistory,
            );
            setConversations(cleanedConversationHistory);
        }

        const selectedConversation = localStorage.getItem('selectedConversation');
        if (selectedConversation) {
            const parsedSelectedConversation: Conversation =
                JSON.parse(selectedConversation);
            const cleanedSelectedConversation = cleanSelectedConversation(
                parsedSelectedConversation,
            );
            setSelectedConversation(cleanedSelectedConversation);
        } else {
            setSelectedConversation({
                id: 1,
                name: 'New conversation',
                messages: [],
                prompt: DEFAULT_SYSTEM_PROMPT,
                folderId: 0,
                index: {
                    indexName: '',
                    indexType: '',
                },
            });
        }
    }, [homeServerSideApiKeyIsSet]);

    return (
        <>
            <Head>
                <title>ChatFiles</title>
                <meta name="description" content="ChatGPT but better."/>
                <meta name="viewport"
                      content="height=device-height ,width=device-width, initial-scale=1, user-scalable=no"/>
                <link rel="icon" href="/favicon.ico"/>
            </Head>
            {selectedConversation && (
                <main
                    className={`flex h-screen w-screen flex-col text-sm text-white dark:text-white ${lightMode}`}
                >
                    <div className="fixed top-0 w-full sm:hidden">
                        <Navbar
                            selectedConversation={selectedConversation}
                            onNewConversation={handleNewConversation}
                        />
                    </div>

                    <div className="flex h-full w-full pt-[48px] sm:pt-0">
                        <Chat
                            conversation={selectedConversation}
                            messageIsStreaming={messageIsStreaming}
                            keyConfiguration={keyConfiguration}
                            loading={loading}
                            onSend={handleSend}
                            onUpdateConversation={handleUpdateConversation}
                            onEditMessage={handleEditMessage}
                            stopConversationRef={stopConversationRef}
                            handleKeyConfigurationValidation={handleKeyConfigurationValidation}
                            role='User'
                        />
                    </div>

                </main>
            )}
        </>
    );
};
export default Home;

export const getServerSideProps: GetServerSideProps = async ({locale}) => {
    return {
        props: {
            homeServerSideApiKeyIsSet: !!process.env.OPENAI_TYPE,
            ...(await serverSideTranslations(locale ?? 'en', [
                'common',
                'chat',
                'sidebar',
                'markdown',
            ])),
        },
    };
};
