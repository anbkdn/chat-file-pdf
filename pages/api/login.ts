import { NextApiRequest, NextApiResponse } from 'next';
import {getAdminSystem} from "@/utils/app/configuration";
import jwt from "jsonwebtoken";
import {SECRET_KEY} from "@/utils/app/const";

// eslint-disable-next-line import/no-anonymous-default-export
export default (req: NextApiRequest, res: NextApiResponse) => {
    const {username, password} = req.body;
    const admin = getAdminSystem()

    if (!username || !password) {
        res.status(401).send('Unauthorized');
        return;
    }

    if (username === admin.username && password === admin.password) {
        const token = jwt.sign({username, password}, SECRET_KEY || '', { expiresIn: '24h' });

        return res.status(200).json({ message: 'Authentication successful', token });
    } else {
        res.status(401).send('Unauthorized');
    }
};
