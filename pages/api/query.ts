import type {NextApiRequest, NextApiResponse} from 'next'
import {getExistingVectorStore, getUserAgent, saveUserAgent, updateUserAgent} from "@/utils/vector";
import {getModel} from "@/utils/openai";
import {loadQAStuffChain} from "langchain/chains";
import { getKeyConfiguration } from '@/utils/app/configuration';
import path from "path";
import fs from "fs";
import {RATE_LIMIT} from "@/utils/app/const";

export const config = {
    api: {
        bodyParser: false,
    }
};
function isExpired(expireDate: any) {
  return new Date(expireDate).getTime() < new Date().getTime();
}
async function handleTraffic(req: NextApiRequest, res: NextApiResponse) {
  const ua = req.headers['user-agent'];
  const uaExist= await getUserAgent(ua)
  const currentDate = new Date();
  // Add one day (24 hours) to the current date
  const nextDate = new Date(currentDate);
  nextDate.setDate(currentDate.getDate() + 1);
  if(!uaExist.data?.length) {
    return saveUserAgent({name: ua, count: 1, expire: nextDate}).then()
  } else {
    const row = uaExist.data[0]

    if (isExpired(row.expire) && row?.count === Number(RATE_LIMIT)) {
      updateUserAgent({...row, count: 1, expire: nextDate}).then()
    }
    if (row?.count < Number(RATE_LIMIT) && !isExpired(row.expire)) {
      updateUserAgent({...row, count: row.count + 1}).then()
    } else {
      return res.status(429).json({ errorMessage: "To many request in per day" });
    }
  }
}

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    // @ts-ignore
        console.log("beginning handler");
        const keyConfiguration = getKeyConfiguration(req);
        const message: string = req.query.message as string;
        const folderPath = path.join(process.cwd(), 'uploads');
        const files = fs.readdirSync(folderPath);
        const fileNames = files.filter((file) => {
            const filePath = path.join(folderPath, file);
            return fs.statSync(filePath).isFile();
        });
        const indexName = fileNames[0].split('.')[0] as string

        console.log({indexName: indexName})

        //:TODO check agent to block request if over > 5 request
        await handleTraffic(req,res)

        console.log("handler chatfile query: ", message, indexName);
        const vectorStore = await getExistingVectorStore(keyConfiguration, indexName);

        const documents = await vectorStore.similaritySearch(message, 1);
        const llm = await getModel(keyConfiguration, res);
        const stuffChain = loadQAStuffChain(llm);

        try {
            stuffChain.call({
                input_documents: documents,
                question: message,
            }).then(res => {
              console.log(res,'res')
            }).catch(console.error);
            // res.status(200).json({ responseMessage: chainValues.text.toString() });
        } catch (e) {
            console.log("error in handler: ", e);
            res.status(500).json({ errorMessage: (e as Error).toString() });
        }

}

export default handler;