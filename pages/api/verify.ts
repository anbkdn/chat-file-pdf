import { NextApiRequest, NextApiResponse } from 'next';
import {getAdminSystem} from "@/utils/app/configuration";
import jwt from "jsonwebtoken";
import {SECRET_KEY} from "@/utils/app/const";

// eslint-disable-next-line import/no-anonymous-default-export
export default (req: NextApiRequest, res: NextApiResponse) => {
  try {
    console.log("beginning verify handler");

    const admin = getAdminSystem()
    const token = req.headers.authorization?.split(' ')[1];

    if (!token) {
      return res.status(401).json({ error: 'Unauthorized' });
    }

    const decoded = jwt.verify(token, SECRET_KEY as string) as any;
    if (!decoded) {
      return res.status(401).json({ error: 'Invalid token' });
    }

    console.log(decoded,'@@@')

    const { username } = decoded;
    if (username === admin.username) {
      return res.status(200).json({ message: 'Authentication successful'});
    } else {
      res.status(401).send('Unauthorized');
    }
  } catch (e: any) {
    return res.status(500).json({ error: e?.message });
  }
};
